import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MySingletonClass {

    private final List<String> shopList;
    private static MySingletonClass instance = new MySingletonClass();

    private MySingletonClass() {
        shopList = new ArrayList<>();

        if (instance != null) {
            throw new RuntimeException("Use getInstance");
        }
    }

    public static MySingletonClass getInstance() {

        if (instance == null) {
            synchronized (MySingletonClass.class) {
                if (instance == null) {
                    instance = new MySingletonClass();
                }
            }
        }
        return instance;
    }

    public boolean verification(String item) {

        for (String string : shopList) {
            if (firstVerification(item)) {
                if (item.equals(string)) {
                    return false;
                }
            } else return false;
        }
        return true;
    }

    private boolean firstVerification(String string) {
        if (string.startsWith("Food") || string.startsWith("Other")) {
            return true;
        } else {
            return false;
        }
    }

    public boolean addSimple(String string) {

        if (shopList.isEmpty()) {
            if (firstVerification(string)) {
                shopList.add(string);
                return true;
            } else {
                return false;
            }
        }
        if (verification(string) == true) {
            shopList.add(string);
            return true;
        }
        return false;
    }

    public int listSize() {
        return shopList.size();
    }

    public void getAllFood() {

        List<String> copy = shopList.stream()
                .filter(s -> s.startsWith("Food"))
                .collect(Collectors.toList());

        System.out.println(copy.toString());
    }

    public void getAllOther() {

        List<String> copy = shopList.stream()
                .filter(s -> s.startsWith("Other"))
                .collect(Collectors.toList());

        System.out.println(copy.toString());
    }
}
