public class main {

    public static void main(String[] args) {

        MySingletonClass instance = MySingletonClass.getInstance();

        System.out.println(instance.listSize());

        instance.addSimple("joão");

        instance.addSimple("Foods");

        instance.addSimple("Otherstuff");

        instance.addSimple("FoodsBread");

        instance.addSimple("OthernotOther");

        instance.addSimple("Gonçalo");

        System.out.println(instance.listSize());

        instance.getAllFood();

        instance.getAllOther();

    }
}
